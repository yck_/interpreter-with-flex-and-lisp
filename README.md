# lexer-with-flex-and-lisp
This reporsitory contains 2 different implementation of home-brewed, full flavored language G++ (rules are provided in the pdf file). 

The yacc_Interpreter folder contains implementation using flex and yacc tool.
The lisp_Interpreter folder contains implementation using lisp language from stratch.


For yacc_Interpreter;

make

./gpp_interpreter.out

or 

./gpp_interpreter.out inputfile


For lisp_Interpreter;

clisp gppinterpreter.lisp

or

clisp gppinterpreter.lisp inputfile
